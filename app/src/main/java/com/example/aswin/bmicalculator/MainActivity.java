package com.example.aswin.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtWeight;
    EditText txtHeight;
    TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtWeight = findViewById(R.id.txtWeight);
        txtHeight = findViewById(R.id.txtHeight);
        tvResult = findViewById(R.id.tvResult);
    }

    public void calculateBMI(View view) {
        float weight = Float.parseFloat(txtWeight.getText().toString());
        float height = Float.parseFloat(txtHeight.getText().toString()) / 100;

        float bmi = weight / (height * height);

        String result = "BMI = " + bmi + ", ";

        if(bmi < 18.5) {
            result += "You are underweight";
        } else if(bmi > 25) {
            result += "You are overweight";
        } else {
            result += "You have a normal weight";
        }

        tvResult.setText(result);
    }
}
